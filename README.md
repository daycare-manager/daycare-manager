Project moved to [here](https://git.allthefallen.ninja/daycare-manager/daycare-manager)

# daycare-manager

You can play the contents of this repo from this link: https://daycare-manager.gitlab.io/daycare-manager/

## Compiling
This is a twine project run using twee files, compiled via tweego (https://www.motoslave.net/tweego/). It uses the SugarCube story format.