Config.history.controls = false; //can't go back on your decisions can you

Config.macros.maxLoopIterations = 100; // allow only 100 iterations

Config.saves.slots = 10; //gives you 10 save slots

Config.saves.autosave = "autosave"; // autosaves on passages tagged with "autosave"